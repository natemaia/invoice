% vim:ft=tex

\ProvidesClass{invoice}

\LoadClass[12pt]{article}

\usepackage[letterpaper,hmargin=0.79in,vmargin=0.79in]{geometry}
\usepackage[parfill]{parskip}
\usepackage{fp}
\usepackage{longtable}

\pagestyle{empty}
\linespread{1.5}
\setlength{\doublerulesep}{\arrayrulewidth}

\def\totalcost{0}\def\subcost{0}\def\vatcost{0}

\newcommand*{\format}[1]{\FPround{\temp}{#1}{2}\temp}

\newenvironment{invoiceTable}{%

	\newcommand*{\feerow}[2]{%
		\FPadd{\tempa}{\subcost}{##2}
		\global\let\subcost\tempa%
		\FPadd{\tempa}{\totalcost}{##2}
		\global\let\totalcost\tempa%
		##1 & & \$\format{##2}
		\\
    }

	\newcommand*{\vatrow}[2]{%
		\FPmul{\tempa}{\subcost}{##2}
		\global\let\vatvalue\tempa%
		\FPadd{\tempa}{\totalcost}{\vatvalue}
		\global\let\totalcost\tempa%
		##1 & \FPmul{\ob}{##2}{100}\format{\ob}\% & \$\format{\vatvalue}
		\\
	}

	\newcommand{\subtotal}{%
		\bfseries Subtotal & & \bfseries \$\format{\subcost}
		\\
    }

    \setlength{\tabcolsep}{0.8ex}
	\setlength\LTleft{0pt}
	\setlength\LTright{0pt}
	\begin{longtable}{@{\extracolsep{\fill}\hspace{\tabcolsep}} l r r }
    \bfseries Description & & \multicolumn{1}{c}{\bfseries Amount} \\*
    \hline
    \endhead%
}{
	\hline
    \bfseries Total & & \bfseries \$\format{\totalcost} \\
    \end{longtable}
}
